/*! 
  This file contains functions that calculates oxygen line emissivities 
 */

#include "head.h"

void OxygenLineEmissivity(double tmp, double *Q)
{
 static int count=0;
 double ptmp, t, e1, e2, e3, e4;
 static double *pT, *OVII, *OVIII;


 if (pT == NULL){
     pT		= calloc(200, sizeof(double));
     OVII	= calloc(200, sizeof(double));
     OVIII	= calloc(200, sizeof(double));
//     print1("> Reading Oxygen line emissivity.\n", prank);
     FILE *flines = fopen("input_tables/Oxygen.linelist","r");
     if (flines == NULL){ print1("! input_tables/Oxygen.linelist could not be found. Exiting Now.\n", prank); exit(1);}
     while(fscanf(flines,"%lf %lf %lf %lf %lf", &t, &e1, &e2, &e3, &e4) != EOF){
         pT[count]	= t;
	 OVII[count]	= (e1+e2+e3)*1.1e9;		// in units of photons cm^3 s^{-1}
	 OVIII[count]	= e4*9.55e8;
	 count++;
     }
     fclose(flines);
 }

 ptmp = log10(tmp);

 Q[0]	= 0.0;
 Q[1]	= 0.0;

 if (ptmp <= 5.0 || ptmp >= 9.0) return;

// printf("ptmp=%2.2f, count=%d\n", ptmp, count);
 // Finding the temperature location in the table
 int klo, kmid, khi;
 double ptmid, dptmp;
 klo = 0;
 khi = count - 1;
 while (klo != (khi - 1)){
//     printf("klo=%d, kmid=%d, khi=%d, ptmid=%2.2f\n", klo, kmid, khi, ptmid);
     kmid = (klo + khi)/2;
     ptmid = pT[kmid];
     if (ptmp <= ptmid){
         khi = kmid;
     }else if (ptmp > ptmid){
         klo = kmid;
     }

 }
// char test_char[100];
// sprintf(test_char,"klo=%d, khi=%d, OVII[klo]=%2.4e, OVII[khi]=%2.4e at pT=%2.4f\n", klo, khi, OVII[klo], OVIII[khi], ptmp);
// print_test(test_char, prank);
 
 dptmp  = pT[khi]-pT[klo];
 Q[0]	= OVII[khi]*(ptmp - pT[klo])/dptmp + OVII[klo]*(pT[khi]-ptmp)/dptmp;
 Q[1]	= OVIII[khi]*(ptmp - pT[klo])/dptmp + OVIII[klo]*(pT[khi]-ptmp)/dptmp;
 
// sprintf(test_char,"Q[0] = %2.2e at T=%2.4e\n", Q[0], tmp);
// print_test(test_char, prank);
// exit(1);

 return;
}
