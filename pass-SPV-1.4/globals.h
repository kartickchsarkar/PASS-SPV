
#define ARRAY_1D(nx,type)          (type    *)Array1D(nx,sizeof(type))
#define ARRAY_2D(nx,ny,type)       (type   **)Array2D(nx,ny,sizeof(type))
#define ARRAY_3D(nx,ny,nz,type)    (type  ***)Array3D(nx,ny,nz,sizeof(type))
#define ARRAY_4D(nx,ny,nz,nv,type) (type ****)Array4D(nx,ny,nz,nv,sizeof(type))

/* *******************************************************
 * Function definitions 
 * ********************************************************/

int main(int, char **);
FILE *ChooseOutputName(char*, int);
void AllocateMemories(int);
void WriteBegingingOfCode(int);
void StoreGridFile(FILE *, int);
void StoreData(FILE *, int);
void SaveMekalModels(int);
void IntegrateAlongLOS(int, int, FILE *, int);
void FreeMemories(int);
void CombineOutput(FILE *, int);
void print1(char *, int );
void print_test(char *, int);

void FindLocalHost (double, double, double, int *);
void FindLocalQOfHost (int *, double *);
void FindOutboxQOfHost (double, double, double, double *);
void FindLBQOfHost(int *, double *);
double LocalRhoOfHost (int *);
double OutboxRhoOfHost (double, double, double);
void FindLocalSpectraOfHost (int *, double **) ;
void FindOutboxSpectraOfHost(double, double, double, double **);
void FindLBSpectraOfHost (double, double, double, double **);

double Lambda (double, double);
void XrayLambda (double, double, double *);
double Lambda_freefree (double, double, double);
char *Host(double, double, double);
void OxygenLineEmissivity(double, double *);

double    *Array1D (int, size_t);
double   **Array2D (int, int, size_t);
double  ***Array3D (int, int, int, size_t);
double ****Array4D (int, int, int, int, size_t);
void FreeArray1D (double *);
void FreeArray2D (int, double **);
void FreeArray3D (int, int, double ***);
void FreeArray4D (int, int, int, double ****);
double **NullArray2D (double, double, int);

double RoundMet(double);
double RoundTmp(double);
double Rend (double, double);
double lmax (double, double);

double phi (double, double);
double d_hot (double, double);
void AnalyseData(char *);
void GetRunningFileNumber(char *, char *);

/* ************************************
 * Global variables 
 * ************************************ */
double *d, **grid, *x1left, *x1right, *x2left, *x2right, *x3left, *x3right, ****dmekal;
double ***SBproc, ***SB;
int prank, nprocs;
long int g_usedMem;
time_t start_time;
time_t end_time;


