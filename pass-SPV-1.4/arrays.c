
#include "head.h"


double *Array1D( int NX , size_t dsize )
/* 1D array definition */ 
{
 int i;
 double *v;

 v = malloc(NX*dsize);
 
 return v;
}


/* ****************************************************** */
double **Array2D ( int NY, int NX, size_t dsize )
/* 2D array definition */
{
 int j;
 double **v;
 v = malloc(NY*sizeof(double *));

 for (j=0; j<NY; j++) 
     v[j] = Array1D(NX, dsize);

 return v;
}

/* ******************************************************** */
double ***Array3D (int NZ, int NY, int NX, size_t dsize)
/* 3D array definition */
{
 int k;
 double ***v;
 v = malloc(NZ*sizeof(double **));

 for (k=0; k<NZ; k++)
      v[k] = Array2D(NY, NX, dsize);

 return v;
}


/* ********************************************************* */
double ****Array4D (int PX, int NZ, int NY, int NX, size_t dsize)
/* 4D array definition */
{
 int l;
 double ****v;
 v = malloc(PX*sizeof(double ***));

 for (l=0; l<PX; l++)
      v[l] = Array3D(NZ, NY, NX, dsize);

 return v;
}

double **NullArray2D (double NY, double NX, int dsize)
{
 int i, j, k;
 double **v;
 v	= Array2D(NY, NX, dsize);
 for (j=0; j<NY; j++ )
 for (i=0; i<NX; i++ )
     v[j][i]	= 0.0;

 return v;

}


/* ********************************************************** *
 * Functions to free higher dimensional arrays 
 * ********************************************************** */
void FreeArray1D (double *v)
{
 free(v);
}

/* ********************************************************* */
void FreeArray2D (int NY, double **v)
{
 int i;
 for (i=0; i<NY; i++) free( v[i] );
 free(v);
}

/* ******************************************************* */
void FreeArray3D (int NZ, int NY, double ***v)
{
 int i;
 for(i=0; i<NZ; i++) FreeArray2D (NY, v[i]);
 free(v);
}


/* ********************************************************* */
void FreeArray4D (int PX, int NZ, int NY, double ****v)
{
 int i;
 for (i=0; i<PX; i++) FreeArray3D (NZ, NY, v[i]);
 free(v);
}
