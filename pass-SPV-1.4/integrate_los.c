
#include "head.h"
void WriteSpecFile(double **, double, double);


void IntegrateAlongLOS(int m, int n, FILE *fout, int prank)
/* 
 * Integrates quantities along a particular line of sight LOS.
 * The algorithm is as followed
 * i) start from solar poition which is at a distance of 8.5 kpc 
 *     from the Galactic centre. 
 * ii) move along the LOS by a differential length ds. 
 * iii) find the local host i.e. the closest computational grid.
 * iv) calculate the required values/spectrum at that grid 
 * v) integrate the quantities/spectrum along that LOS. In case of
 *     Mekal model, we consider only 0.5-2.0 keV energies.
 * ----------------------------------------------------------
 * VARIABLE:
 * m [I]	= index for longitute
 * n [I] 	= index for latitude
 * fout [I/O]   = file handle in which the spectrum of a data is written.
 * ---------------------------------------------------------------------
 */
{

 int k, s, *lh;
 double l, b, M, rho,*I, *Q;
 double xp, yp, zp, rhost;
 double **spec, **intspec;
 char specfile[50], *host, str[50];
 FILE *fspec;
 char str1[100];
 static int first_timer = 1;
 char str4[100];
 
 lh     	= calloc(3, sizeof(int));
 spec   	= Array2D(2000, 2, sizeof(double));
 intspec	= Array2D(2000, 2, sizeof(double));
 Q 		= calloc(2,sizeof(double));
 I 		= calloc(2,sizeof(double));

 l  = m*dl;
 b  = n*db;

 fprintf (fout, "%3.3e %3.3e ", l, b);

 Q[0]	= Q[1]   = 0.0;
 I[0]	= I[1]   = 0.0;
// rho = 0.0;
 
 #if MEKAL
 for (s = 0; s < 2000; s++){		// initiate the integrated spectrum to zero
     intspec[s][0]  = 0.1 + 0.002*s;
     intspec[s][1] = 0.0;
 }
 #endif

 for (k = 0; k<100000; k++){               // Proceed along LOS 

     xp     = R0 - k*ds*cos(b*3.14/180.0)*cos(l*3.14/180.0);	// Find new position on the LOS
     yp     = -k*ds*cos(b*3.14/180.0)*sin(l*3.14/180.0);
     zp     = k*ds*sin(b*3.14/180.0);
     host   = Host(xp, yp, zp);			// decides the host status

    #if MEKAL == 1	// only for mekal model
    if ( strcmp(host,"InsideLocBubble") == 0){
        FindLocalHost(xp, yp, zp, lh);
        FindLBSpectraOfHost(lh, spec);
    }else if ( strcmp(host,"InsideSimBox") == 0){ 				// if inside the computation box
        FindLocalHost (xp, yp, zp, lh);	
        FindLocalSpectraOfHost (lh, spec);
    }else if (strcmp(host,"InsideExtBox") == 0){				// if outside the computation box
        FindOutboxSpectraOfHost(xp, yp, zp, spec);
    }else{
        break;
    }
    #else
    if ( strcmp(host,"InsideLocBubble") == 0){
        FindLocalHost(xp, yp, zp, lh);
        FindLBQOfHost(lh, Q);
    }else if ( strcmp(host,"InsideSimBox")==0){ 
        FindLocalHost (xp, yp, zp, lh);
        FindLocalQOfHost (lh, Q);
    }else if ( strcmp(host,"InsideExtBox")==0){
        FindOutboxQOfHost(xp, yp, zp, Q);
    }else{
        break;
    }
    #endif

    //  integrating along the LOS 
    #if MEKAL
    for (s = 0; s < 2000; s++)       
        intspec[s][1]       += ds*spec[s][1];    // adding the photons or counts along LOS
    #endif

    I[0]       = I[0] + ds*Q[0];
    I[1]       = I[1] + ds*Q[1];
 }      // end of k loop, to integrate along LOS

 #if MEKAL == 1
 I[0] = I[1] = 0.0;
 for (s=200; s<=950; s++)                                  // from E=0.5 keV to E = 2.0 keV
     I[0] +=  intspec[s][1]*intspec[s][0]*0.002;     // N(E)*E*dE
 for (s=951; s< 2000; s++)                      	   //for 2.0-4.0 keV 
     I[1] +=  intspec[s][1]*intspec[s][0]*0.002;
 #endif

 fprintf (fout, "%3.4e	%3.4e\n", I[0], I[1]); 
 
 #if MEKAL & WRITE_SPECTRA
 WriteSpecFile(intspec, l, b);
 #endif

 free(lh); free(Q); free(I);
 FreeArray2D(2000, spec);
 FreeArray2D(2000, intspec);
 
}


/* ************************************************** */
void WriteSpecFile(double **intspec, double l, double b)
/*
 * Writes the 0.5-2.0 keV spectrum in mekal/l%db%d.mdl files in binary format.
 * First 2000*8 bytes are for energies and last 2000*8 bytes are the 
 * corresponding energy/flux/counts
 */

{
 int s, nEbins, offset;
 double *E, *flux;
 char specfile[50];
 FILE *fspec;
 nEbins = 2000; 	// number of bins in the energy spectrum

 E 	= calloc(nEbins, sizeof(double));
 flux 	= calloc(nEbins, sizeof(double));
 for (s=0; s<nEbins; s++){
     E[s]	= intspec[s][0];
     flux[s]	= intspec[s][1];
 }

 sprintf(specfile, "mekal/l%db%d.mdl", ( int)(l*10.0), ( int)(b*10.0) );


 fspec = fopen(specfile, "wb");
 
 offset = 0;
 fseek(fspec, 0, SEEK_SET);
 fwrite(E, sizeof(double), nEbins, fspec);

 offset = sizeof(double)*(nEbins);
 fseek(fspec, offset, SEEK_SET);
 fwrite(flux, sizeof(double), nEbins, fspec);

/* fspec = fopen(specfile, "w");
 for (s=0; s<nEbins; s++)
     fprintf(fspec, "%12.6e %12.6e\n", E[s], flux[s]);
*/



 fclose(fspec);
 free(E); free(flux);
}
