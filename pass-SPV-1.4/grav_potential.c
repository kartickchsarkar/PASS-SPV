
#include "head.h"

/* *********************************************************
 * Gravitational potentials
 * Unit : (100 km/s)^2
 * *********************************************************** */

/* subroutine to return the potential at any point */
double phi (double R, double z)
{
 R *= kpc;
 z *= kpc;
 return phi_nfw(R,z) + phi_mn(R,z);// + phi_b(R,z);
}

double phi_nfw (double R, double z)
{
 double r;
 r = sqrt(R*R+z*z);
 return - CONST_G*Mvir/(fc*rs)*log(1.0+sqrt(r*r+a1*a1)/rs)/(sqrt(r*r+a1*a1)/rs);
}

double phi_mn (double R, double z)
{
 return - CONST_G*Mdisc/sqrt( R*R+(a+sqrt(z*z+bp*bp))*(a+sqrt(z*z+bp*bp)) );
}

double phi_b(double R, double z)
{
 return -CONST_G*Mb/sqrt(R*R+z*z+ab*ab);
}

/* subroutine to find the dphi/dR at any point */
double dphi_nfwdR (double R)

{
 return - CONST_G*Mvir/(fc*rs)*R/((R*R+a1*a1)*(1.0+sqrt(R*R+a1*a1)/rs)) \
        + CONST_G*Mvir/fc*R/pow(R*R+a1*a1,3.0/2.0)*log(1.0+ sqrt(R*R+a1*a1)/rs);
}

double dphi_mndR (double R)
{
 return CONST_G*Mdisc*R/pow(R*R+(a+bp)*(a+bp),3.0/2.0);
}

double dphi_bdR (double R)
{
 return CONST_G*Mb*R/pow(R*R+ab*ab,3.0/2.0);
}

/*double v_rot(double R)
{
 R = absolute(R*kpc);
 return sqrt( R*( dphi_nfwdR(R)+dphi_mndR(R)+dphi_bdR(R) ) )/(g_unitVelocity);

}
*/

/* Subroutine to find the cold phase density */
double d_cold (double R, double z)
{
 double phirz, phir0, phi00;

 phirz	= phi(R,z);
 phir0  = phi(R,0.001);
 phi00	= phi(0.001,0.001);

 return d0_cold*exp(-1.0/(csc*csc)*( phirz-phi00 -f*f*(phir0-phi00)) );
}


/* subroutine to find hot phase density */
double d_hot (double R, double z)
{
 double phirz, phi00, phir0;

 phirz  = phi(R,z);
 phir0	= phi(R, 0.001);
 phi00  = phi(0.001,0.001);

 return d0_hot*exp(-1.0/(csh*csh)*( phirz-phi00 - fh*fh*(phir0-phi00) ));
}

double d_cmz (double R, double z)
{
 double phirz, phir0, phi00;

 phirz	= phi(R,z);
 phi00	= phi(0.001,0.001);

 return d0_cmz*exp(-1.0/(cscmz*cscmz)*(phirz-phi00) );
}
/* subrotine for returning absolute value of the input */
double absolute (double y)
{
 return (y >= 0.0 ) ? y : -y ;
}



