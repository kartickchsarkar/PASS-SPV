#include "head.h"

void FindLBQOfHost (int *ii, double *Q)
{
 int i, j, k;
 double rho, v1, v2, v3, prs_i, met, tmp, unit;

 Q[0] = 0.0;
 Q[1] = 0.0;

 rho = ne_LB;
 tmp = T_LB;

  #if OTH
 unit = kpc/(4.0*3.14);
 double *dmp = calloc(200, sizeof(double));
 OxygenLineEmissivity(tmp, dmp);
 Q[0] = unit*rho*rho*dmp[0];                    // in units of photons cm^{-2} s^{-1} Sr^{-1}
 Q[1] = unit*rho*rho*dmp[1];
 free(dmp);
 #endif

 return;

}

/* *************************************************************************** */
void FindLBSpectraOfHost(double x, double y, double z, double **Q)
{
 int i, j, k, l, s, imet, itmp;
 double R, rho, tmp, ptmp, met, ne, unit, Ebeg, Eend, met_pr, ptmp_pr;

 x /= unit_len;
 y /= unit_len;
 z /= unit_len;
 R      = sqrt(x*x+y*y);

 rho    = ne_LB;	            // in m_p/cc
 met    = 0.2;
 ne     = rho;                         // in electrons/cc
 tmp    = T_LB;                        // in K

 unit   = 3.08e21/(4.0*3.14)*(1.6e-9)*1.e-14;
 ptmp   = log10(tmp);                   // getting the 'p'ower of temperature

 met_pr    = RoundMet(met);            // rounding the met and ptmp to its closest existing value
 ptmp_pr   = RoundTmp(ptmp);
 imet   = ( int)(( met_pr -0.20)/0.20); // converting the rounded met,ptmp to the indicies
 itmp   = ( int)(( ptmp_pr -6.0)/0.050)+1;

 Ebeg   = 0.1;

 for(k=0; k<2000; k++){      // defining the bins of the spectra from 0.1-3.0 keV
    Q[k][0]     = Ebeg + 0.002*k;
    Q[k][1]     = 0.0;
 }

 if (itmp < 0 || tmp < 4e5) return;

 for (l=0; l<200; l++){                 //  finding the starting point of 0.1 in the mekal spectra
     if (dmekal[imet][itmp][l][0] >= Ebeg){
      break;
      }
 }

 for (s=0; s<2000; s++)                 // storing the output spectra (photons/s/keV/sr/cm^3) in a smaller (0.1-4.0 keV) array.
     Q[s][1]    = 1.27*ne*ne*unit*dmekal[imet][itmp][l+s-1][1];

}
