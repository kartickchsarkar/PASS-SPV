

#ifndef HEAD_H
#define HEAD_H

#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<time.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<unistd.h>

/* Define macros */
#define VERSION 1.0

#define YES     1
#define NO      0

#define PARALLEL	YES

#if PARALLEL
  #include<mpi.h>
#endif

#define CONST_G 6.67e-8
#define kB      1.38e-16
#define CONST_PI 3.14159265359
#define pc      3.08e18
#define kpc     (1.e3*pc)
#define Msun    2.e33
#define mp	 1.67e-24
#define km      1.e5
#define yr      3.15e7
#define Myr     (1.e6*yr)

#define DIMENSIONS	2
#define COMPONENTS	3

#if DIMENSIONS == 1
    #define EXPAND(a,b,c)       a
#elif DIMENSIONS == 2
    #define EXPAND(a,b,c)       a b
#elif DIMENSIONS == 3 || COMPONENTS == 3
    #define EXPAND(a,b,c)       a b c
#endif

/* ***************************************************************************
 * Galactic model definitions
 * => required only in case extended box is considered otherwise do not modify
 * *************************************************************************** */

#include "grav.h"

/* ***************************************************
 * Simulation details
 ***************************************************** */
#define CARTESIAN	NO
#define SPHERICAL	YES
#define CYLINDRICAL	NO
#define CONSIDER_EXT_BOX	NO

#if SPHERICAL
  #define rbox	15.0
#elif CYLINDRICAL
  #define Rbox	15.0
  #define zbox	3.14159
#endif

#if CONSIDER_EXT_BOX
  #define rout	200.0
#endif

#define R0	8.5
#define r_LB	0.2
#define ne_LB	4.e-3
#define T_LB	1.2e6

#define nx	1024
#define ny	512
#define nz	1
#define nvar	8

#define idx_rho	0
#define idx_vx1	1
#define idx_vx2	2
#define idx_vx3	3
#define idx_prs	4
#define idx_met	5
#define unit_len	1.0		//in kpc
#define unit_rho	1.0		//in m_p/cc
#define unit_v	100.0		// km/s

/* **********************************************
 * output emission type specifications
 ************************************************ */
#define FREEFREE	NO
#define MEKAL	NO
#define MEKAL_NOSPECTRA	NO
#define GAMMA_HAD	NO
#define GAMMA_LEP	YES
#define SYNC23G	NO
#define COLDEN	NO
#define EM	NO
#define OTH	NO



/* **********************************************
 * Projected surface specification
 ************************************************ */
#define g_mtotal	512
#define g_ntotal	512
#define ds	0.1
#define dl	(180.0/(g_mtotal*1.0))
#define db	(180.0/(g_ntotal*1.0))
#if SPHERICAL
   #define dy0         (rbox/(g_mtotal*1.0))
   #define dz0         (rbox/(g_ntotal*1.0))
#elif CYLINDRICAL
  #define dy0           (Rbox/(g_mtotal*1.0))
  #define dz0           (zbox/(g_ntotal*1.0))
#endif

/* *****************************************************
 * Output filename specifications
 * ***************************************************** */
#define g_logFile	YES
#define WRITESPECTRA	NO
#define AUTOMATIC_FILENAME	YES

static char *MekalModelFolder	= "/home/sarkar/softwares/pimms/models/";
static char *GridFile           = "../grid1.out";
static char *g_outfile          = "output/user.txt";



#include "globals.h"



#endif

