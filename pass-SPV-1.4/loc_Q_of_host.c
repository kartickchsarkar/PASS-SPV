
#include "head.h"

double GetIC_Emissivity(char *, double);

//void FindLocalQOfHost (double x, double y, double z, double *Q)
void FindLocalQOfHost (int *ii, double *Q)
/* ***********************************************
 * calculates the observables of the host grid 
 * *********************************************** */
{
 int i, j, k;
 double rho, tmp, vr, vth, vphi, v, v_t, prs_i, prs_w, prs_tot, p_cr, met, ne, ni, E, dE=0.05, unit;
 double v1, v2, v3, trc, u_th;
 static int first_time = 1;
 static double eps1, eps2;
 char str[100];

 Q[0] = 1.e-60;
 Q[1] = 1.e-60;

 i = ii[0];
 j = ii[1];
 k = ii[2];

 rho    = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_rho]*unit_rho;                   // in m_p/cc
 EXPAND( v1   = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_vx1]*unit_v;,			// in km/s
	 v2   = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_vx2]*unit_v;,			// in km/s
	 v3   = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_vx3]*unit_v;)			// in km/s

 prs_i    = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_prs]*unit_rho*unit_v*unit_v*1.67e-14;	// in dyne/cm^2
 trc	= d[i + j*nx + k*nx*ny + nx*ny*nz*5];
 if (idx_met == 0){
     met = 0.2;
 }else{
     met    = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_met];
 }
 tmp = (prs_i/rho)*(mu/1.38e-16) ;
// tmp    = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_temp];				// in K

 ne     = rho/1.15;					// rho/(mue*mp) in particles/cc
 ni     = rho/1.248;					// rho/(mui*mp) in particles/cc

 v = sqrt( EXPAND(v1*v1, + v2*v2, + v3*v3) );					// actual velocity
 #if CARTESIAN
   x = (x1left[i]+x1right[i])/2.0;
   y = (x2left[j]+x2right[j])/2.0;
   ph = atan(y/x);
   v_t = sqrt( EXPAND(v1*v1*(1.0+sin(ph))*(1.0+sin(ph)), + v2*v2*(1.0-cos(ph))*(1.0-cos(ph)), +0.0 ) );
 #else
   v_t = sqrt( EXPAND(v1*v1, + v2*v2, + 0.0) );					// non-azimuthal velocity
 #endif

// prs_w	= rho*v*v*1.67e-14;
// prs_tot = prs_i + prs_w;

// p_cr = 3.5e-10*pow(rho/0.063, 4.0/3.0);
 u_th = 3./2.*prs_i; // + rho*v*v*1.67e-24;				// erg/cm^3

 #if FREEFREE == 1
/* ********************************************** 
 * For emission in x-ray pure bremstrahlung emission
 * ********************************************* */
 unit = kpc/(4.0*3.14);
 Q[0] = unit*ne*ni*Lambda_freefree(tmp, 0.5,2.0);
 Q[1] = unit*ne*ni*Lambda_freefree(tmp, 2.0,10.0);
 return;
 #endif

 #if MEKAL_NOSPECTRA
 unit = 3.08e21/(4.0*3.14);
 double *lam;
 lam = calloc(2, sizeof(double));
 XrayLambda(tmp, met, lam);

 Q[0] =  unit*ne*ni*lam[0];
 Q[1] =  unit*ne*ni*lam[1];

 free(lam);
 return;
 #endif

/* *************************************************** 
 * For hadronic emission in gamma-ray 
 * ************************************************** */
 #if GAMMA_HAD == 1
  unit = 1.4e-17*3.08e21;				// 1.2e-16*kpc for hadronic gamma-ray
  if (v_t > 20.0 && met > 0.9){
     Q[0] = prs_tot*ne*unit;
  }else{
     Q[0] = 1.e-34;
  }
 #endif


/* *************************************************** 
 * For leptonic emission in gamma-ray 
 * ************************************************** */
 #if GAMMA_LEP
  if (first_time){
     eps1  = GetIC_Emissivity("/home/sarkar/plutoworks/NPS-asymmetry/spectrum/spectrum_2p2_3p2.txt", 50.0);
     eps2  = GetIC_Emissivity("/home/sarkar/plutoworks/NPS-asymmetry/spectrum/spectrum_2p4_3p4.txt", 50.0);
     first_time = 0;
  }
  unit = kpc/(4*3.14);
  double zz;
  zz = x1left[i]*cos(x2left[j]);
//  printf("zz=%2.4f, i, j=%d,%d\n", zz,i,j);
//  if (v_t > 20.0){
//     Q[0] = eps2*u_th*unit;
  if (fabs(zz) >= 0.7  && v_t > 20.){	// avoid the disc and halo emission
     Q[0] = eps2*u_th*unit;
     if (met > 0.5) Q[0] += eps1*u_th*unit;
  }else{
     Q[0] = 1.e-60;
  }
#endif

/* *****************************************************
 * For 23 GHz synchroton emission
 * **************************************************** */
 #if SYNC23G == 1
// unit = 4.0e-21*3.08e21*2.0;				// 2.0 is because of the same reason as leptonic case
 unit	= 3.1e-20*3.08e21;
 if (v_t > 20.0 && met > 0.9){
    Q[0] = pow(prs_tot, 1.8)*unit;          
 }else{
    Q[0] = 1.e-40;
 }
 #endif

 #if COLDEN 	/* For column density */
 unit = 3.08e21;
 Q[0] = rho*unit;
/* if (v_t >= 20.0){
     Q[0] = rho*unit;
 }else{
     Q[0] = 1.e-34;
 }
*/
 #endif


 #if EM == 1	/* For emission measure */
 unit = 3.08e21;
 if (tmp >= 2.4e6 && tmp <= 4.0e6){
     Q[0] = ne*ne*unit;			// cm^{-6}
 }else if (tmp >= 5.e6 && tmp <= 1e8){
     Q[1] = ne*ne*unit;			// cm^{-6}
 }else{
     Q[0] = 0.0;
     Q[1] = 0.0;
 }
 #endif

 #if OTH
 unit = kpc/(4.0*3.14);
 double *dmp = calloc(200, sizeof(double));
 OxygenLineEmissivity(tmp, dmp);
 Q[0] = unit*ne*ni*dmp[0];			// in units of photons cm^{-2} s^{-1} Sr^{-1}
 Q[1] = unit*ne*ni*dmp[1];
 free(dmp);
 #endif

 return;
}


/* ********************************************************************* */
void FindOutboxQOfHost (double x, double y, double z, double *Q)
/* ***********************************************
 * calculates the observables of the host grid 
 * *********************************************** */
{
 int i, j, R;
 double rho, tmp, vr, vth, vphi, v, v_t, prs_i, prs_w, prs_tot, p_cr, met, ne, ni, E, dE=0.05, unit, u_th;
 char str[100];

 Q[0] = 0.0;
 Q[1] = 0.0;
 x /= unit_len;
 y /= unit_len;
 z /= unit_len;

 R      = sqrt(x*x+y*y);

 if (sqrt(x*x+y*y+z*z) <= 50.0){ 
     if (z >= 0.0) rho    = d_hot(R, z);            // in m_p/cc
     else rho	= 0.8*d_hot(R,z);
 }else{
     rho = d_hot(R,z);
 }

 vr	= 0.0;
 vth	= 0.0;
 vphi	= 0.0;
 prs_i  = rho*T_HALO*1.38e-16/(mu*1.67e-24);
 met    = 0.3;
 ne     = rho/1.15;                        // in electrons/cc
 ni	= rho/1.248;
 tmp    = T_HALO;                        // in K

 v = sqrt(vr*vr + vth*vth + vphi*vphi);
 v_t = sqrt (vr*vr + vth*vth);
 prs_w	= rho*v*v*1.67e-24*1.e10;
 prs_tot = prs_i + prs_w;

 p_cr = 3.5e-10*pow(rho/0.063, 4.0/3.0);
 u_th = 2./3.*prs_i;

 #if FREEFREE
/* ********************************************** 
 * For emission in x-ray pure bremstrahlung emission
 * ********************************************* */
 unit = kpc/(4.0*3.14);
 Q[0] = unit*ne*ni*Lambda_freefree(tmp, 0.5,2.0);
 Q[1] = unit*ne*ni*Lambda_freefree(tmp, 2.0,10.0);
 return;
 #endif

 #if MEKAL_NOSPECTRA
 unit = 3.08e21/(4.0*3.14);
 double *lam;
 lam = calloc(2, sizeof(double));
 XrayLambda(tmp, met, lam);
 Q[0] =  unit*ne*ni*lam[0];
 Q[1] =  unit*ne*ni*lam[1];
 free(lam);
 #endif

 #if EM == 1	/* For emission measure */
 unit = 3.08e21;
 if (tmp >= 2.4e6 && tmp <= 4.0e6){
     Q[0] = ne*ne*unit;			// cm^{-6}
 }else if (tmp >= 5.e6 && tmp <= 1e8){
     Q[1] = ne*ne*unit;			// cm^{-6}
 }else{
     Q[0] = 0.0;
     Q[1] = 0.0;
 }
 #endif

 #if COLDEN == 1	/* For emission measure */
 unit = 3.08e21;
 Q[0] = ne*unit;			// cm^{-6}
 #endif

 #if GAMMA_HAD || GAMMA_LEP || SYNC23G  
 Q[0] = 1.e-60;
 Q[1] = 1.e-60;
 #endif

 /* *******************************
  * Put your emissivity here      
  * ******************************* */
 #if OTH
 unit = kpc/(4.0*3.14);
 OxygenLineEmissivity(tmp, Q);
 Q[0]  *= unit*ne*ni;			// in units of photons cm^{-2} s^{-1} Sr^{-1}
 Q[1]  *= unit*ne*ni;
 #endif

 return;
}



double LocalRhoOfHost (int *ii)
/* ***********************************************
 * calculates the EM of the host grid 
 * *********************************************** */
{
 int i, j, k;
 double rho, tmp, v1, v2, v3, v, v_t, prs_i, prs_w, prs_tot, p_cr, met, ne, Q, E, dE=0.05, unit;

 i = ii[0];
 j = ii[1];
 k = ii[2];

 rho    = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_rho]*unit_rho;                   // in m_p/cc
 EXPAND( v1   = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_vx1]*unit_v;,                      // in km/s
         v2   = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_vx2]*unit_v;,                      // in km/s
 	 v3   = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_vx3]*unit_v;)                      // in km/s
 prs_i    = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_prs]*unit_rho*unit_v*unit_v*1.67e-14;   // in dyne/cm^2
 tmp = (prs_i/rho)*(mu/1.38e-16) ;
// tmp    = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_temp];                          // in K

 v = sqrt( EXPAND(v1*v1, + v2*v2, + v3*v3) );                                   // actual velocity
 #if CARTESIAN
   x = (x1left[i]+x1right[i])/2.0;
   y = (x2left[j]+x2right[j])/2.0;
   ph = atan(y/x);
   v_t = sqrt( EXPAND(v1*v1*(1.0+sin(ph))*(1.0+sin(ph)), + v2*v2*(1.0-cos(ph))*(1.0-cos(ph)), +0.0 ) );
 #else
   v_t = sqrt( EXPAND(v1*v1, + v2*v2, + 0.0) );                                 // non-azimuthal velocity
 #endif

 ne     = rho;                                       // in particles/cc
 unit = 3.08e21;
 if (v_t >= 20.0 && tmp > 1.e6)
     Q = rho*unit;
 else
     Q = 1.e-34;

 return Q;

}

/* ****************************************************************** */
double OutboxRhoOfHost (double x, double y, double z)
{
 double R;
 x /= unit_len;
 y /= unit_len;
 z /= unit_len;
 R = sqrt(x*x+y*y);
 
 return d_hot(R,z);
}


void FindLocalSpectraOfHost (int *ii, double **Q)
/* ***********************************************
 * Calculates the spectra (0.1-4.0 keV) of the host grid. 
 * By default it has been set for mekal model.
 * VARIABLES:
 * lh [I]	= X,Y indices of the local host
 * Q [I/O]	= spectra of the host
 * *********************************************** */
{
 int i, j, k, l, s, imet, itmp;
 double rho, prs_i, tmp, ptmp, met, ne,  unit, Ebeg, Eend, met_pr, ptmp_pr;
 double unit_mekal, unit_ff, E;
 char str[50];

 i = ii[0];
 j = ii[1];
 k = ii[2];
  
 rho    = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_rho]*unit_rho;                   // in m_p/cc
 prs_i  = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_prs]*unit_rho*unit_v*unit_v*1.67e-14;                   // in m_p/cc
 if (idx_met == 0){
     met    = 0.2;
 }else{
     met    = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_met];
 }
 tmp        = (prs_i/rho)*(mu/1.38e-16);
// tmp    = d[i + j*nx + k*nx*ny + nx*ny*nz*idx_temp];                    // in K 
 ne     = rho;                                          // in electrons/cc

 unit_mekal   = 3.08e21/(4.0*3.14)*(1.6e-9)*1.e-14;	//  is the grid separation, (..) \
                                                         is the energy value at that grid in erg \
                                                         1.e-14 is the normalisation constant from XPEC.\
							The unit is erg/s/cm^2/sr/keV
 unit_ff = 3.08e21/(4.0*3.14)*2.4e17*8.8e-38;	// erg/s/cm^2/sr/keV

 ptmp   = log10(tmp);			// getting the 'p'ower of temperature

 met_pr    = RoundMet(met); 		// rounding the met and ptmp to its closest existing value
 ptmp_pr   = RoundTmp(ptmp);
 imet   = ( int)(( met_pr -0.20)/0.20);	// converting the rounded met,ptmp to the indicies
 itmp   = ( int)(( ptmp_pr -6.0)/0.050)+1;

 Ebeg	= 0.1;

 for(k=0; k<2000; k++){      // defining the bins of the spectra from 0.1-3.0 keV
    Q[k][0]	= Ebeg + 0.002*k; 
    Q[k][1]	= 0.0; 
 }
 
 if (itmp < 0 || tmp < 4e5 ) return;


 if (tmp <= 1.e8){
     /* storing the output spectra in a smaller (0.1-4.0 keV) array. */
     for (l=0; l<200; l++) //  finding the starting point of 0.1 in the mekal spectra
         if (dmekal[imet][itmp][l][0] >= Ebeg)  
		break;

     for (s=0; s<2000; s++)   Q[s][1]	= 1.27*ne*ne*unit_mekal*dmekal[imet][itmp][l+s-1][1];
 }else if (tmp <= 1e11){
     /* For free-free emission */
     for (s=0; s<2000; s++){
         E = Q[s][0];
         Q[s][1]	= 1.27*ne*ne*unit_ff*pow(tmp, -0.5)*exp(-1.16*E*1.e7/tmp); 
     }
 }else{
     printf("! Warning: Temperature exceeds 10^11 K. The emission physics is unknown here ! \n");
//     exit(1);
 }

 }



/* ******************************************************* */
void FindOutboxSpectraOfHost(double x, double y, double z, double **Q)
/* *******************************************************
 * Getting the outbox (r > simulaion box size) spectra of host.
 * VARIABLES:
 * x,y,z [I]	= X,Y,Z positions of the host
 * Q [I/O]	= Spectra of the host grid.
 * ******************************************************* */

{
 int i, j, k, l, s, imet, itmp;
 double R, rho, tmp, ptmp, met, ne, unit, Ebeg, Eend, met_pr, ptmp_pr;

 x /= unit_len;
 y /= unit_len;
 z /= unit_len;
 R	= sqrt(x*x+y*y);

 rho    = d_hot(R, z)*1.0;            // in m_p/cc
 met    = 0.2;
 ne     = rho;                                          // in electrons/cc
 tmp    = T_HALO;                        // in K

 unit   = 3.08e21/(4.0*3.14)*(1.6e-9)*1.e-14;
 ptmp   = log10(tmp);                   // getting the 'p'ower of temperature

 met_pr    = RoundMet(met);            // rounding the met and ptmp to its closest existing value
 ptmp_pr   = RoundTmp(ptmp);
 imet   = ( int)(( met_pr -0.20)/0.20); // converting the rounded met,ptmp to the indicies
 itmp   = ( int)(( ptmp_pr -6.0)/0.050)+1;

 Ebeg   = 0.1;

 for(k=0; k<2000; k++){      // defining the bins of the spectra from 0.1-3.0 keV
    Q[k][0]     = Ebeg + 0.002*k;
    Q[k][1]     = 0.0;
 }

 if (itmp < 0 || tmp < 4e5) return;

 for (l=0; l<200; l++){                 //  finding the starting point of 0.1 in the mekal spectra
     if (dmekal[imet][itmp][l][0] >= Ebeg){
      break;
      }
 }

 for (s=0; s<2000; s++)                 // storing the output spectra (photons/s/keV/sr/cm^3) in a smaller (0.1-4.0 keV) array.
     Q[s][1]    = 1.27*ne*ne*unit*dmekal[imet][itmp][l+s-1][1];

}


/* ****************************************
 * To get the density for r > rbox
 * *************************************** */
void FindOutboxEMOfHost(double x, double y, double z, double *Q) 
{
 double R, ne, unit = 3.08e21;
 x /= unit_len;
 y /= unit_len;
 z /= unit_len;
 R	= sqrt(x*x+y*y);
 ne	= d_hot(R, z)*1.0;

 Q[0] = 0.0;
 Q[1] = 0.0;

 if (T_HALO >= 2.4e6 & T_HALO <= 4.0e6){
     Q[0] = ne*ne*unit;			// cm^{-6} 
 }
 if (T_HALO >= 5e6 && T_HALO <= 1.e8){
     Q[1] = ne*ne*unit;
 }

 return;
}



/* *********************************** */
double FreeFree(double E, double T)
/* 
 E [I]	= energy in keV;
 T [I]  = temperature in K
*/
{
 return pow(T, -0.5)*exp(-1.16*E*1.e7/T);
}

/* ************************************ */
double GetIC_Emissivity(char *file, double E)
{
 double x, E1, dE_tol; 
 FILE *fp = fopen(file, "r");
 dE_tol = 0.1;			//GeV 

 if (fp == NULL){ printf("could not find file %s.\n Exiting.\n", file); exit(1);}

 while (fscanf(fp, "%lf %lf", &E1, &x) != EOF){
//     printf("E = %2.4f, eps= %2.4e\n", E1, x);
     if (E1 >= E-dE_tol) break;
 }
 fclose(fp);
 
 return x;
}
