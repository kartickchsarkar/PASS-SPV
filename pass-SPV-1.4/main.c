

#include "head.h"

int main(int argc, char *argv[])
/*  
 *Calculates the projected data of our Galaxy if looked from the solar system.
 * The calculations are done in sky coordinates (l,b).
 * ------------------------------------------------------------------
 * This function can calculate projected
 * 1) Mekal X-ray brightness in erg/s/cm^2/sr (option is MEKAL == 1)
 * 2) Free-free X-ray brightness in erg/s/cm^2/sr (option is XRAY == 1)
 * 2) Emission measure in cm^{-2} in 2.4-3.6 keV band (EM ==1)
 * 3) Hadronic gamma-ray brighness in erg/s/cm^2/sr (GAMMA_HAD == 1)
 * 4) Leptonic gamma-ray brighness in erg/s/cm^2/sr (GAMMA_LEP == 1)
 * 5) 23 GHz radio brightness in erg/s/cm^2/sr/Hz (SYNC23G == 1)
 * 6) HI column density in cm^{-2} from Solar vantage point  (EDGEONCOL == 1)
 * 7) HI column density in cm^{-2} from a vantage point parallel to the stellar disc (VANTCOL == 1)
 * 8) Metallicity map.
 * -----------------------------------------------------------------------
 * Last modified: 08.01.2016 by Kartick C Sarkar, email: kcsarkar@rri.res.in
*/

{
 start_time = time(NULL);

 int i, j, m, n;
 double x0, y0, z0, r0, th0, r, theta, modr;
 char filename[50], str2[100];
 FILE *fout; 

 FILE *fbin     = fopen (argv[1],"rb");
 FILE *gridfile = fopen (GridFile,"r");

 /* initiate parallelisation */
 int mbeg, mend, dm, nbeg, nend;
 int status;
 int prank, nprocs;

 MPI_Init(&argc, &argv);	// Initialise MPI and find number and set ranks of processors
 MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
 MPI_Comm_rank(MPI_COMM_WORLD, &prank);

 FILE *fp[nprocs];
 dm	= g_mtotal/nprocs;
 mbeg	= dm*prank;		// dividing the job limits to processors
 mend	= mbeg + dm;


 sprintf(filename, "output/.outfile_%d.tmp",prank);	// Creating ouput filenames for processors
 status = remove(filename);				// remove pre-existing temporary files
 fp[prank] = fopen(filename, "a");
/* Initialisation done*/

 WriteBegingingOfCode(prank);
 fout = ChooseOutputName(argv[1], prank);				// choosing final output filename

 /* Writting the header of the output file */
 fprintf(fout, "# The unit of emission is: erg s^{-1} cm^{-2} sr^{-1}. \n");
 fprintf(fout, "# from file %s\n", argv[1]);

 AllocateMemories(prank); 

 StoreGridFile(gridfile, prank);

 StoreData(fbin, prank);		// Storing the simulation data

 #if MEKAL
   SaveMekalModels(prank);		 /* storing the mekal model */
 #else
   dmekal = NULL;
 #endif

 char msg2[200];
 sprintf(msg2,"> PASS has been parallelised across %d processors.", nprocs);
 print1(msg2, prank); 
 print1("> .................................\n> Starting Computation ...\n", prank); 

 nbeg = -g_ntotal/2.0; nend = g_ntotal/2.0;

 /* calculating the variable wanted; looping over longitudes and latitutes*/
 for (m=mbeg; m<mend; m++){ 		/* l loop for one proc */ 
     for (n=nbeg; n<nend; n++){	/* b loop */
         IntegrateAlongLOS(m, n, fp[prank], prank);
     }
     fprintf (fp[prank],"\n");
     sprintf(str2, " Task completed = %3.3f %%.", (m*100.0/dm));
     print1( str2, prank);
 }                                      

 fclose(fp[prank]);
 FreeMemories(prank);

 #if PARALLEL
 MPI_Barrier(MPI_COMM_WORLD);
 MPI_Finalize();
 #endif

 if (prank == 0){
     CombineOutput(fout, nprocs); 
 }

 return 0;
}

